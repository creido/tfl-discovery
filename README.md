# TFL Discovery

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).


## Getting started

```yarn install```
or
```npm install```

followed by

```yarn start```
or
```npm run start```


## Running tests

Run and watch tests
```yarn test```

Run tests and get code coverage report
```yarn test:report```


## File structure and configuration

I've approached this project from a point of view of scalability and have based the structure on the ducks approach, cite https://medium.freecodecamp.org/scaling-your-redux-app-with-ducks-6115955638be which favours feature over function and keeps redux and react nicely separated.
* create-react-app has been ejected to enable configuration of testing tools (Enzyme).


## Next steps / Future dev

* Remove Router tests - we don't need to test React Router
* State management code could be so much DRYer. Data reducers and data fetching actions are identical and there is definitely the possibility of combining things (maybe using a util like [redux-actions](https://redux-actions.js.org/)) but I have kept things compartmentalised for speed and getting a proof of concept.
* Retrospective unit testing to fill in the gaps and TDD moving forward. Unit tests have been written for a lot of the core functionality. In the later stages tests were much less detailed and even skipped (esp if functionality was duplicated) to save on time. In full development mode I would also be looking at integration testing.
* Get Storybook installed and running to provide visual unit testing
* Transitions and loading states. For simple transitions I go for CSS3 transitions/animations toggled by adding and removing classes but for more complex applications look at GSAP TweenLite.
* There's always scope for better naming e.g.
  * `... servicesMenu/ServicesList.js` would be better as `... servicesMenu/ServicesMenuList.js`
  * `... servicesMenu/ServicesItem.js` would be better as `... servicesMenu/ServicesMenuItem.js`
* Embelish caching/offline experience by adding to the service worker and using IndexedDB or Local Storage (PWA).
* Install and run sass as CSS preprocessor
