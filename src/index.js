import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import './styles/index.css';
import App from './app/views/layout/App';
import registerServiceWorker from './registerServiceWorker';
import store from './app/state/store';

ReactDOM.render(

  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'));

registerServiceWorker();
