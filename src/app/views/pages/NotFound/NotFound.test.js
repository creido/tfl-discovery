import React from 'react';
import {shallow} from 'enzyme';
import NotFound from './NotFound';

describe('NotFound', () => {

  const wrapper = shallow(<NotFound match={'test'}/>);

  it('should render without crashing', () => {
    wrapper;
  });

  it('should match its snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
