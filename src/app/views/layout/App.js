import React from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import Routes from '../../routes/Routes';
import ServicesList from '../components/servicesMenu/ServicesList';

export const App = () => (
  <div className="App">
    <header className="App-header">
      <h1 className="App-title">TFL Discovery</h1>
    </header>
    <Router>
      <main className="app-main">
        <ServicesList />
        <div className="app-content">
          <Routes />
        </div>
      </main>
    </Router>
  </div>
);

export default App;
