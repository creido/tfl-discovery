import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {fetchData} from '../../../state/ducks/serviceStatus/operations';
import {ServicesItem} from './ServicesItem';
import {CycleHireSearchResults} from "../cycleHire/CycleHireSearchResults";

const mapStateToProps = state => ({serviceStatusItems: state.serviceStatus.items});

export class ServicesList extends Component {

  componentDidMount() {
    this.props.fetchData()
  }

  render() {
    const {serviceStatusItems} = this.props;

    return (
      <nav className="services">
        <ul className="services__list">
          {serviceStatusItems.map(service => {
            /**
             * TODO: abstract `hasDisruption` and `hasNightService` out into a Container component
             * containing Services logic or as util functions for easier testing and use elsewhere
             *
             * Added here as opposed to passing more service data into ServicesItem so props stay small
             */
            const hasDisruption = service.lineStatuses && service.lineStatuses.some(item => item.statusSeverity !== 10);
            const hasNightService = service.serviceTypes && service.serviceTypes.some(item => item.name === 'Night');

            return (
              <ServicesItem
                key={service.id}
                id={service.id}
                name={service.name}
                hasDisruption={hasDisruption}
                hasNightService={hasNightService} />
              );
            }
          )}
          <ServicesItem id={'cycle-hire'} name={'Cycle hire'} />
        </ul>
      </nav>
    );
  }
}

ServicesList.propTypes = {
  serviceStatusItems: PropTypes.array,
};

export default connect(mapStateToProps, {fetchData})(ServicesList);
