import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';
import {CycleHireSearchResults} from "../cycleHire/CycleHireSearchResults";

// TODO: fix application of activeClassName
export const ServicesItem = ({id, name, hasNightService, hasDisruption}) => (
  <li className={`services__item services__item--${id}`}>
    <NavLink
      className={`services__link services__link--${id}`}
      activeClassName="services__link--active"
      to={`/services/${id}`}>
      {name}
    </NavLink>
    <span className="icons">
      {hasNightService && (
        <span className="icon icon--night">Night service</span>
      )}
      {hasDisruption && (
        <span className="icon icon--disruption">Disruption</span>
      )}
    </span>
  </li>
);

CycleHireSearchResults.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  hasNightService: PropTypes.bool,
  hasDisruption: PropTypes.bool,
};

export default ServicesItem;
