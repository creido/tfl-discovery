import React from 'react';
import {shallow} from 'enzyme';
import {ServicesItem} from './ServicesItem';

const SELECTORS = {
  // TODO: replace selector strings with object keys
};

describe('ServicesItem', () => {

  const mockProps = {
    id: 'central',
    name: 'Central',
    hasNightService: false
  };

  const mockPropsWithNightService = {
    id: 'central',
    name: 'Central',
    hasNightService: true
  };

  const mockPropsWithDisruption = {
    id: 'district',
    name: 'District',
    hasDisruption: true
  };

  const wrapper = shallow(<ServicesItem {...mockProps} />);

  it('renders without crashing', () => {
    wrapper;
  });

  it('contains NavLink component', () => {
    expect(wrapper.find('NavLink')).toHaveLength(1);
  });

  it('indicates if this line has a night service IF hasNightService is in props and is true', () => {
    const wrapperNightService = shallow(<ServicesItem {...mockPropsWithNightService} />);

    expect(wrapper.find('.icon--night-service')).toHaveLength(0);
    expect(wrapperNightService.find('.icon--night')).toHaveLength(1);
  });

  it('indicates if this line has any disruptions', () => {
    const wrapperDisruption = shallow(<ServicesItem {...mockPropsWithDisruption} />);

    expect(wrapper.find('.icon--disruption')).toHaveLength(0);
    expect(wrapperDisruption.find('.icon--disruption')).toHaveLength(1);
  });

});


