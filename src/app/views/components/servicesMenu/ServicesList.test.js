import React from 'react';
import {shallow} from 'enzyme';
import {ServicesList} from './ServicesList';
import {ServicesItem} from './ServicesItem';

describe('ServicesList', () => {

  const mockProps = {
    fetchData: () => {},
    serviceStatusItems: [
      {id: 'central', name: 'Central', lineStatuses: [{statusSeverity: 9}], serviceTypes: []},
      {id: 'dlr', name: 'DLR', lineStatuses: [{statusSeverity: 10}], serviceTypes: []},
      {id: 'victoria', name: 'Victoria', lineStatuses: [{statusSeverity: 9}], serviceTypes: []}
    ]
  };

  const wrapper = shallow(<ServicesList {...mockProps} />);

  it('renders without crashing', () => {
    wrapper;
  });

  // expect length + 1 due to Cycle Hire link
  it('renders service names as ' + (mockProps.serviceStatusItems.length + 1) + ' list items', () => {
    expect(wrapper.find(ServicesItem)).toHaveLength(4);
  });

  it('should match its snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('TODO: hasDisruption test');

  it('TODO: hasNightService test');
});


