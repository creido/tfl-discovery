import React from 'react';
import PropTypes from 'prop-types';

import ServiceDisruptionItem from './ServiceDisruptionItem';
import SETTINGS from '../../utils/settings';

export const ServiceDisruptionList = ({disruptions}) => (
  <div>
    <h2>{SETTINGS.SERVICE_DISRUPTIONS}</h2>
    <ul>
      {
        disruptions.map((item, index) =>
          item.statusSeverity !== 10 &&
          <ServiceDisruptionItem key={index} {...item} />
        )
      }
    </ul>
  </div>
);

ServiceDisruptionItem.propTypes = {
  disruptions: PropTypes.array,
};

export default ServiceDisruptionList;
