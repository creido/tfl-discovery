import React from 'react';
import PropTypes from 'prop-types';
import {ServiceDetail} from "../serviceDetail/ServiceDetail";

export const ServiceDisruptionItem = ({reason, statusSeverityDescription}) => (
  <li>
    <strong>{statusSeverityDescription}:</strong> {reason}
  </li>
);

ServiceDisruptionItem.propTypes = {
  reason: PropTypes.string,
  statusSeverityDescription: PropTypes.string,
};

export default ServiceDisruptionItem;
