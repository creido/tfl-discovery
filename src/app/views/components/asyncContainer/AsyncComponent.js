import React, {Component} from 'react';

/**
 * Async loading wrapper component
 *
 * @param importComponent
 * @returns Child component
 * @constructor
 */
export const AsyncComponent = (importComponent) => {

  return class extends Component {
    state = {
      component: null
    }

    componentDidMount() {
      importComponent()
        .then(cmp => {
          this.setState({component: cmp.default});
        })
        .catch(err => {
          console.log('Error with AsyncComponent loading', err);
        });
    }

    render() {
      const C = this.state.component;
      return C ? <C {...this.props}/> : null;
    }
  }

};

export default AsyncComponent;
