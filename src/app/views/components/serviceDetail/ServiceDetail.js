import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import ServiceDisruptionList from '../serviceDisruptions/ServiceDisruptionList';
import SETTINGS from '../../utils/settings';
import {CycleHireSearchResults} from "../cycleHire/CycleHireSearchResults";

const mapStateToProps = () => (state, ownProps) => ({
  service: state.serviceStatus.items.find(item =>
    item.id === ownProps.match.params.id)
});

export const ServiceDetail = ({match, service}) => {

  /**
   * TODO: Fix async loading
   * HOC AsyncComponent was added so `service && ` check wasn't needed!
   */
  const serviceId = service && service.id;
  const serviceName = service && service.name;

  return (
    <section className={`service-detail service-detail--${serviceId}`}>
      <h1>Service Detail: {serviceName}</h1>
      {
        service && (service.lineStatuses.some(item => item.statusSeverity !== 10) ?
          (
            /**
             * list of every current disruption’s description, extracted from the reason value on each
             * object inside the lineStatuses array with a statusSeverity value different than 10.
             */
            <ServiceDisruptionList disruptions={service.lineStatuses} />
          ) :
          (
            <p>Current status: {SETTINGS.NO_SERVICE_DISRUPTIONS}</p>
          )
        )
      }
    </section>
  );
};

ServiceDetail.propTypes = {
  service: PropTypes.object,
};

export default connect(mapStateToProps)(ServiceDetail);
