import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {fetchData, updateSearchQuery} from '../../../state/ducks/cycleHire/operations';

const mapStateToProps = state => ({cycleHire: state.cycleHire});

const mapDispatchToProps = {fetchData, updateSearchQuery};

export class CycleHireForm extends Component {

  handleInputChange = event => {
    const val = event.target.value

    // update search query in controlled component
    this.props.updateSearchQuery(val);
  };

  handleSubmit = event => {
    event.preventDefault();

    // kick off api call
    this.props.fetchData();
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit} className="cycle-hire__form">
        <label htmlFor="cycle-hire-input">Search for a bike point</label>
        <input id="cycle-hire-input"
               onChange={this.handleInputChange}
               type="text"/>
        <button type="submit">Find now</button>
      </form>
    )
  }
}

CycleHireForm.propTypes = {
  cycleHire: PropTypes.object,
  fetchData: PropTypes.func,
  updateSearchQuery: PropTypes.func,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CycleHireForm);

/**
 * Response:
 * An array of objects, each representing a bike point that matched the search term.
 *
 * Empty array if none was found.
 * Example:
 * [
 *   {
 *     "id": "BikePoints_76",
 *     "commonName": "Longford Street, The Regent's Park",
 */
