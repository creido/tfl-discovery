import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import CycleHireSearchResultsItem from './CycleHireSearchResultsItem';

const mapStateToProps = state => ({results: state.cycleHire.data.items})

/**
 * Display the results as a list, showing the id of the bike point
 * (digits on the end of the id value) followed by its commonName value
 * and then by its coordinates inside a ();
 *
 * Example: “76 Longford Street, The Regent's Park (51.525595,  -0.144083)”
 */
export const CycleHireSearchResults = ({results}) => {
  return (
    <div>
      <p>Your search returned {results.length} results</p>
      <ul className="bike-point__list">
        {
          results.map(item =>
            <CycleHireSearchResultsItem key={item.id} {...item} />
          )
        }
      </ul>
    </div>
  );
};

CycleHireSearchResults.propTypes = {
  results: PropTypes.array,
};

export default connect(mapStateToProps)(CycleHireSearchResults);
