import React from 'react';
import CycleHireForm from './CycleHireForm';
import CycleHireSearchResults from './CycleHireSearchResults';

export const CycleHire = () => (
  <section>
    <h1>Cycle Hire</h1>
    <CycleHireForm />
    <CycleHireSearchResults />
  </section>
);

export default CycleHire;
