import React from 'react';
import PropTypes from 'prop-types';

const getIdNumber = str => str.substring(str.indexOf('_') + 1);

export const CycleHireSearchResultsItem = ({id, commonName, lat, lon}) => (
  <li className="bike-point__item">
    <span className="bike-point__id">{getIdNumber(id)}</span> <span className="bike-point__name">{commonName}</span> <span className="bike-point__coords">({lat}, {lon})</span>
  </li>
);

CycleHireSearchResultsItem.propTypes = {
  id: PropTypes.string,
  commonName: PropTypes.string,
  lat: PropTypes.number,
  lon: PropTypes.number,
}

export default CycleHireSearchResultsItem;
