const SETTINGS = {
  NO_SERVICE_DISRUPTIONS: 'No service disruptions',
  SERVICE_DISRUPTIONS: 'Service currently suffering disruptions',
};

export default SETTINGS;
