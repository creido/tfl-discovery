import React from 'react';
import {Route, Switch} from 'react-router-dom';

import AsyncComponent from '../views/components/asyncContainer/AsyncComponent';
import CycleHire from '../views/components/cycleHire/CycleHire';
import LandingPage from '../views/pages/LandingPage/LandingPage';
import NotFound from '../views/pages/NotFound/NotFound';

const AsyncServiceDetail = AsyncComponent(() => {
  return import('../views/components/serviceDetail/ServiceDetail');
});

const Routes = () => {
  return (
    <Switch>
      <Route exact path='/' component={LandingPage} />
      <Route path="/services/cycle-hire" component={CycleHire}/>
      <Route path='/services/:id' component={AsyncServiceDetail} />
      <Route component={NotFound} />
    </Switch>
  );
};

export default Routes;
