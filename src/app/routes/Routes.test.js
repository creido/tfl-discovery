import React from 'react';
import {mount, shallow} from 'enzyme';
import {MemoryRouter} from 'react-router-dom';
import LandingPage from '../views/pages/LandingPage/LandingPage';
import NotFoundPage from '../views/pages/NotFound/NotFound';
import {App} from '../views/layout/App';

describe('App Router', () => {

  /**
   * TODO: Fix breaking tests - ran out of time
   *
   * ```
   * Invariant Violation: Could not find "store" in either the context or props of "Connect(ServicesList)".
   * Either wrap the root component in a <Provider>, or explicitly pass "store" as a prop to "Connect(ServicesList)".
   * ```
   */

  it('invalid path should redirect to \'Page not found\'', () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={[ '/random' ]}>
        <App/>
      </MemoryRouter>
    );
    expect(wrapper.find(LandingPage)).toHaveLength(0);
    expect(wrapper.find(NotFoundPage)).toHaveLength(1);
  });

  it('valid path should not redirect to \'Page not found\'', () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={[ '/' ]}>
        <App/>
      </MemoryRouter>
    );
    expect(wrapper.find(LandingPage)).toHaveLength(1);
    expect(wrapper.find(NotFoundPage)).toHaveLength(0);
  });

});
