/**
 * GET data from a given API
 *
 * @param   url            {string}  GET url of the API call
 * @param   fetchFunction  {string}  Name of the fetch method to use
 * @returns {Promise<any>}
 */

// Promise based component left in for posterity :)

// export const getData = (url, fetchFunction = window.fetch) => {
//   return fetchFunction(url)
//     .then(response => response.json())
//     .catch(err => {
//       throw new Error('error')
//     });
// };

export const getData = async (url, fetchFunction = window.fetch) => {
  try {
    const response = await fetchFunction(url);
    return await response.json();
  } catch(err) {
    throw new Error('error');
  }
};

export default getData;
