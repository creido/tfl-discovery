import getData from './getData';

describe('#getData() utility ', () => {

  const fakeAPI = 'https://this-is-not-an-api.com';

  const fakeData = [
    {id: 'district', lineStatuses: [{statusSeverityDescription: 'Good Service'}]},
    {id: 'london-overground', lineStatuses: [{statusSeverityDescription: 'Disruption'}]},
  ];

  it('should correctly receive data', async () => {
    let isFakeFetchCalled = false;

    const fakeFetch = () => {
      isFakeFetchCalled = true;

      return Promise.resolve({
        json: () => Promise.resolve(fakeData)
      });
    };

    const expected = fakeData;

    expect.assertions(3);

    try {
      const data = await getData(fakeAPI, fakeFetch);

      expect(isFakeFetchCalled).toBe(true);
      expect(data).toBeDefined();
      expect(data).toEqual(fakeData);
    } catch(err) {
      console.error('getData() call failed:', err);
    }

  });

  it('should throw an error on fail', async () => {
    let isFakeFetchCalled = false;

    const fakeFetch = () => {
      isFakeFetchCalled = true;

      return Promise.reject();
    };

    expect.assertions(2);

    try {
      await getData(fakeAPI, fakeFetch);
    } catch (err) {
      expect(isFakeFetchCalled).toBe(true);
      expect(err).toEqual(Error('error'));
    }
  });

});
