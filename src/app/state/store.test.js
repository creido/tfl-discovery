import store from './store';

describe('store', () => {

  it('should initialise', () => {
    const actual = store.getState();
    const expected = {
      cycleHire: {
        data: {
          isFetching: false,
          items: []
        },
        ui: {
          searchQuery: 'Search'
        }
      },
      serviceStatus: {
        isFetching: false,
        items: []
      }
    };

    expect(actual).toEqual(expected);
  });

});
