import types from './types';

const dataRequest = () => {
  return {
    type: types.DATA_REQUEST
  }
};

const dataFailure = err => {
  return {
    type: types.DATA_FAILURE,
    payload: err
  }
};

const dataSuccess = data => {
  return {
    type: types.DATA_SUCCESS,
    payload: data
  }
};

export const updateSearchQuery = val => {
  return {
    type: types.SEARCH_QUERY_UPDATE,
    payload: val
  }
};

export default {
  dataRequest,
  dataFailure,
  dataSuccess,
  updateSearchQuery,
};
