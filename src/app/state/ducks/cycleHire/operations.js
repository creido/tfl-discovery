/**
 * Operations or async action creators
 */
import actions from './actions';
import getData from '../../utils/getData';

// TODO: move this out of this file into a global
const API_ENDPOINT = 'https://api.tfl.gov.uk/BikePoint/Search?query=';

/**
 * TODO: replace Promise then with async/await
 *
 * @param   query       {string} Search query from a form
 * @returns {Function}
 */
export const fetchData = () => {
  return (dispatch, getState) => {
    // sets reducer isFetching prop to true
    // TODO: use this to prevent multiple API calls being made at once
    dispatch(actions.dataRequest());

    const {searchQuery} = getState().cycleHire.ui;
    const url = `${API_ENDPOINT}${searchQuery}`;

    return getData(url)
      .then(data => {
        return dispatch(actions.dataSuccess(data))
      })
      .catch(err => {
        dispatch(actions.dataFailure(err))
      });
  };
};

// Single action passed straight through
export const updateSearchQuery = actions.updateSearchQuery;
