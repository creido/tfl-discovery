import {combineReducers} from 'redux';
import types from './types';


const INITIAL_STATE = {
  DATA: {
    isFetching: false,
    items: []
  },
  UI: {
    searchQuery: 'Search'
  }
};

// Keep separation of api data and app user interface by having multiple reducers

const cycleHireUIReducer = (state = INITIAL_STATE.UI, action) => {

  switch(action.type) {

    case types.SEARCH_QUERY_UPDATE:
      return {
        ...state,
        searchQuery: action.payload
      };

    default:
      return state;
  }
};


const cycleHireDataReducer = (state = INITIAL_STATE.DATA, action) => {

  switch(action.type) {

    case types.DATA_REQUEST:
      return {
        ...INITIAL_STATE.DATA,
        isFetching: true
      };

    case types.DATA_FAILURE:
      return {
        ...state,
        isFetching: false
      };

    case types.DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        items: [...state.items, ...action.payload]
      };

    default:
      return state;
  }
};

const reducer = combineReducers({
  data: cycleHireDataReducer,
  ui: cycleHireUIReducer
});

export default reducer;
