import reducer from './reducers';
// import store from '../../store';
import configureStore from 'redux-mock-store';

import types from './types';

const middlewares = [];
const mockStore = configureStore(middlewares);

const INITIAL_STATE = {
  isFetching: false,
  items: []
};

const store = mockStore(INITIAL_STATE);

describe('Service Status Reducer', () => {

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(INITIAL_STATE);
  });

  it('should be updated on dispatching DATA_REQUEST action', () => {
    const action = {
      type: types.DATA_REQUEST,
      payload: {error: 'There was an error'}
    };

    const actual = reducer(undefined, action);

    const expected = {
      isFetching: true,
      items: []
    };

    expect(actual).toEqual(expected);
  });

  it('should be updated on dispatching DATA_FAILURE action', () => {
    const action = {
      type: types.DATA_FAILURE,
      payload: {error: 'There was an error'}
    };

    const actual = reducer(undefined, action);

    const expected = {
      isFetching: false,
      items: []
    };

    expect(actual).toEqual(expected);
  });

  it('should update store on dispatching DATA_SUCCESS action', () => {
    const action = {
      type: types.DATA_SUCCESS,
      payload: [
        {id: 'bakerloo'},
        {id: 'central'},
        {id: 'dlr'},
      ]
    };

    const actual = reducer(undefined, action);

    const expected = {
      isFetching: false,
      items: [
        {id: 'bakerloo'},
        {id: 'central'},
        {id: 'dlr'},
      ]
    };

    expect(actual).toEqual(expected);
  });

});
