/**
 * Operations or async action creators
 */
import _groupBy from 'lodash.groupby';
import _sortBy from 'lodash.sortby';
import actions from './actions';
import getData from '../../utils/getData';

const API_ENDPOINT = 'https://api.tfl.gov.uk/Line/Mode/tube,overground,dlr/Status?detail=true'

/**
 * TODO: replace Promises then with async/await
 *
 * @param url
 * @returns {Function}
 */
export const fetchData = (url = API_ENDPOINT) => {
  return (dispatch) => {
    // sets reducer isFetching prop to true
    dispatch(actions.dataRequest());

    return getData(url)
      .then(data => {

        // TODO: extract data sorting into another function to be called in separately
        // data should be sorted by modeName first then name
        const sortedGroups = _sortBy(_groupBy([...data], 'modeName'));

        // flatten array groups
        const sortedData = [].concat(...sortedGroups);

        return dispatch(actions.dataSuccess(sortedData))
      })
      .catch(err => {
        dispatch(actions.dataFailure(err))
      });
  };
};

export default {
  fetchData,
};
