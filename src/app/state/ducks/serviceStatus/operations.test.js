import {fetchData} from './operations';
import types from './types';

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const mockResult = [{id: 'dlr'}, {id: 'central'}];

describe('#fetchData() async action creator', () => {

  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  it('creates DATA_SUCCESS action when data fetching has completed successfully', () => {
    fetchMock.getOnce('https://api.tfl.gov.uk/Line/Mode/tube,overground,dlr/Status?detail=true', mockResult);

    const expectedActions = [
      { type: types.DATA_REQUEST },
      { type: types.DATA_SUCCESS, payload: [{id: 'dlr'}, {id: 'central'}] }
    ];

    const store = mockStore({ items: [] });

    store.dispatch(fetchData())
      .then(() => { // return of async actions
        expect(store.getActions()).toEqual(expectedActions)
      })
      .catch(err => {
        console.error(err);
      });

  });

  it('creates DATA_FAILURE action when data fetching fails');

});
