import types from './types';

const dataRequest = () => {
  return {
    type: types.DATA_REQUEST
  }
};

const dataFailure = err => {
  return {
    type: types.DATA_FAILURE,
    payload: err
  }
};

const dataSuccess = data => {
  return {
    type: types.DATA_SUCCESS,
    payload: data
  }
};

export default {
  dataRequest,
  dataFailure,
  dataSuccess
};
