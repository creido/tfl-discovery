import types from './types';

const INITIAL_STATE = {
  isFetching: false,
  items: []
};

const serviceStatusReducer = (state = INITIAL_STATE, action) => {

  switch(action.type) {

    case types.DATA_REQUEST:
      return {
        ...INITIAL_STATE,
        isFetching: true
      };

    case types.DATA_FAILURE:
      return {
        ...state,
        isFetching: false
      };

    case types.DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        items: [...state.items, ...action.payload]
      };

    default:
      return state;
  }
};

export default serviceStatusReducer;
