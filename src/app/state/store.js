import {createStore, applyMiddleware, combineReducers} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import cycleHireReducer from './ducks/cycleHire/reducers';
import serviceStatusReducer from './ducks/serviceStatus/reducers';

const reducer = combineReducers({
  cycleHire: cycleHireReducer,
  serviceStatus: serviceStatusReducer,
});

const store = createStore(
  reducer,
  composeWithDevTools(
    applyMiddleware(thunk)
  )
);

export default store;
